crafty-bitmaps (1.0-3) unstable; urgency=medium

  * QA upload.

  * Added d/gbp.conf to describe branch layout.
  * Updated vcs in d/control to Salsa.
  * Updated d/gbp.conf to enforce the use of pristine-tar.
  * Updated Standards-Version from 3.9.1 to 4.7.0.
  * Use wrap-and-sort -at for debian control files
  * Trim trailing whitespace.
  * Use versioned copyright format URI.
  * Bump debhelper from deprecated 7 to 13.
  * Set debhelper-compat version in Build-Depends.
  * Change priority extra to priority optional.
  * Remove badly placed newline in d/copyright.
  * Flag in d/control that build do not need root.

 -- Petter Reinholdtsen <pere@debian.org>  Thu, 02 May 2024 08:22:19 +0200

crafty-bitmaps (1.0-2) unstable; urgency=medium

  * QA upload.
  * Set maintainer to Debian QA Group <packages@qa.debian.org>. (see: #835317)

 -- Marcos Talau <talau@debian.org>  Wed, 09 Nov 2022 14:43:57 -0300

crafty-bitmaps (1.0-1) unstable; urgency=low

  * Initial release (Closes: #620939) ITP.
  * Thanks to Mike Rohleder <mike@rohleder.de> I found the motivation
    for fixing this bug. The images for the crafty annotation mode
    have been requested since Nov 2004. (Closes: #281936)
  * The upstream source bitmaps.tgz contained .gif binary files and
    a temporary tar archive. These files were not needed and have
    been removed. The orig.tar.gz only contains the .bm files,
    which are converted to .png.
  * The source now contains only XBM graphics, which are converted
    to png with the help of "convert" from the imagemagick package.

 -- Oliver Korff <ok@xynyx.de>  Mon, 04 Apr 2011 11:27:56 +0200
